import requests
from bs4 import BeautifulSoup

class LAB5:
    poems = []
    def get_poems(self):
        for poem in range(1, 71):
            req = requests.get("https://literat.ug.edu.pl/roxolan/00{}.htm".format(poem)) if poem < 10 else requests.get("https://literat.ug.edu.pl/roxolan/0{}.htm".format(poem))
            text = BeautifulSoup(req.content, "lxml").text.split()
            self.poems += text[2:len(text)-2]


if __name__ == '__main__':
    lab5 = LAB5()
    poems = lab5.get_poems()
    print(poems)