from LAB06.movie_data import MovieData
from LAB06.movie_scrapper import MovieScrapper
from LAB06.movie_predictions import MoviePredictions

def prowadzacy_widzial_film(movies_to_recommend: list):
    widziane_filmy_prowadzacy = movie_data.df[movie_data.df['osoba'] == 'Paweł Czapiewski']['film']
    widziane_filmy_prowadzacy = [film for film in widziane_filmy_prowadzacy]
    for movie in movies_to_recommend:
        if movie in widziane_filmy_prowadzacy:
            raise AssertionError('Brak spełnienia zadania: Musibyć film nieznany')

if __name__ == '__main__':
    '''
    1. Load data from CSV
    2. Find Cartesian logic in there (Colaborative filtering)
    3. Zaproponuj 5 filmów dla prowadząceg
    '''
    movie_data = MovieData("LAB06/Oceny_Filmow_BGT.csv")
    movie_predictions = MoviePredictions(movie_data)

    movies_to_recommend = [
        'Django',
        'Breaking Bad',
        'Taken',
        'Jackie Brown',
        'Toy Story'
    ]
    prowadzacy_widzial_film(movies_to_recommend)

    # Polecenie 5 filmów prowadzącemu
    prowadzacy = "Paweł Czapiewski"
    for movie in movies_to_recommend:
        ocena_filmu_dla_prowadzacego = movie_predictions.predict(prowadzacy, movie)
        dane_filmu = MovieScrapper(movie)

        print(f"\nFILM: {movie}")
        print(f"OCENA DOPASOWANIA: {ocena_filmu_dla_prowadzacego}")
        print(f"\n-- METADANE FILMU --")
        print(f"OBSADA: {dane_filmu.actors()}")
        print(f"OCENA: {dane_filmu.average_score()}")
        print(f"FABULA: {dane_filmu.story_line()}")





