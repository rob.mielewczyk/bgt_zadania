import pandas as pd

def calculate_recommended_movie_based_on_user_ratings(df: pd.DataFrame, movie_title: str):
    # 1. Get data set with (rating, amount of ratings)
    avg_ratings = df.groupby('film')['ocena'].mean()
    amount_of_ratings = df.groupby('film')['ocena'].count()
    df_ratings = pd.DataFrame({"rating": avg_ratings, "amount of ratings": amount_of_ratings})

    # 2. Get user rating correlation for a specified movie
    df_ = df.loc[:, ['osoba', 'film', 'ocena']]
    osoba_film_matrix = pd.pivot_table(df_, columns='film', index='osoba', values='ocena')

    movie_watched = osoba_film_matrix[movie_title]
    li = []
    for i, col in enumerate(osoba_film_matrix.columns):
        li.append(movie_watched.corr(osoba_film_matrix.iloc[:, i]))
    li = pd.Series(li)

    # Step 3 - Return dataframe
    df = pd.DataFrame({'movie':pd.Series(osoba_film_matrix.columns), 'correlation': li, 'amount of ratings': pd.Series(amount_of_ratings)})
    recomendation_set = df[df['amount of ratings'] >= 2].sort_values(by=['correlation', 'amount of ratings'],
                                                                     ascending=False)
    recomended_movies = recomendation_set['movie'].values
    print(recomended_movies)

def calulate_correlation(df: pd.DataFrame):

    # Step 1 - Create correlation table (Person, movie)
    df = df.loc[:, ['osoba', 'film', 'ocena']]
    osoba_film_matrix = pd.pivot_table(df, columns='film', index ='osoba', values ='ocena')

    # Step 2 - perfom corelation
    li = []
    for i, col in enumerate(osoba_film_matrix.columns):
        li.append(osoba_film_matrix[col].corr(osoba_film_matrix.iloc[:, i]))
    li = pd.Series(li)

    # Step 3 - Return dataframe
    number_of_ratings = [amount_of_ratings(df, movie_name) for movie_name in osoba_film_matrix.columns]
    df = pd.DataFrame({'movie': osoba_film_matrix.columns, 'correlation': li, 'number of ratings': number_of_ratings})

    recomendation_set = df[df['number of ratings'] >= 2].sort_values(by=['correlation', 'number of ratings'], ascending=False)
    recomended_movies = recomendation_set['movie'].values

    print(recomended_movies)

