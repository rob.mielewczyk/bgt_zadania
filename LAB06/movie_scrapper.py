from bs4 import BeautifulSoup
import requests


class MovieScrapper:
    '''
    Podstawowy Web Scrapper do filmweba utworzony (19.12.2020).
    '''
    def __init__(self, movie_title: str):
        '''
        Creates Movie MetaData object based on movie title requested from: filmweb.pl
        '''
        self.html = self.get_html(movie_title)

    @staticmethod
    def get_html(movie_title: str):
        # Change space to plus sign (Filmweb requires this)
        movie_title = movie_title.replace(' ', '+')
        url = f"https://www.filmweb.pl/search?q={movie_title}"
        film_previes_html = requests.get(url).content

        # Get correct movie link from the list
        soup = BeautifulSoup(film_previes_html, 'html.parser')
        movie_link = soup.find('a', {"class": "filmPreview__link"})['href']

        #request the correct movie
        url = f"https://www.filmweb.pl{movie_link}"
        movie_html = requests.get(url).content
        return movie_html

    def genre(self):
        pass

    def story_line(self):
        soup = BeautifulSoup(self.html, 'html.parser')
        story_line = soup.find('div', {"class": "filmPosterSection__plot"})
        return story_line.contents[0]

    def average_score(self):
        soup = BeautifulSoup(self.html, 'html.parser')
        score = soup.find('span', {"class": "filmRating__rateValue"}).contents[0]
        return score


    def actors(self):
        soup = BeautifulSoup(self.html, 'html.parser')
        actors = soup.find('div', {"class": "crs__wrapper"}).find_all('div', {"class": "crs__item"})
        actor_list = []
        for actor in actors: #type: BeautifulSoup
            try:
                actor_name = actor.find('span').find('a').contents[0]
                actor_list.append(actor_name)
            except(AttributeError):
                '''
                Sometimes web page renders differently with more junk, instead of writting
                more exact web scrapper I'm silently ignoring error thus doing nothing with
                invalid data
                '''
        return actor_list