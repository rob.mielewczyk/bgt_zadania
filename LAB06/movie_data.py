import pandas as pd

class MovieData():
    def __init__(self, path: str):
        self.df = self.load_data(path)

    def check_if_exists(self, person, movie) -> bool:
        if person not in self.df.osoba.values:
            print("Tej osoby nie ma w bazie")
            return False

        if movie not in self.df.film.values:
            print("Tego filmu nie ma w bazie")
            return False
        return True

    @staticmethod
    def load_data(path: str) -> pd.DataFrame:
        '''
        first column: People
        other columns: Tuple (Movie, Ocena)
        I'm changing it here so that it becomes (Person, Movie, Rating)
        '''
        data = pd.read_csv(path, header=None)
        rows_count = data.shape[0]
        temp_rows = []
        for row in range(rows_count):
            osoba = data.iloc[row, 0]
            filmy = data.iloc[row, 1::2]
            oceny = data.iloc[row, 2::2]

            number_of_rated_movies = len(filmy)
            filmy = [film for film in filmy]
            oceny = [ocena for ocena in oceny]
            for i in range(number_of_rated_movies):
                temp_rows.append({'osoba': osoba, 'film': filmy[i], 'ocena': oceny[i]})

        df = pd.DataFrame(temp_rows, columns=['osoba', 'film', 'ocena'])
        return df

    def amount_of_ratings(self, title: str) -> int:
        '''
        Returns amount of ratings based on movie title as a key
        '''
        try:
            return self.df['film'].value_counts()[title]
        except(KeyError):
            raise KeyError('Ten film nie istnieje w bazie !!!')