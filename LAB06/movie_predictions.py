import pandas as pd
from surprise import KNNWithMeans, Dataset, Reader
from surprise.model_selection import GridSearchCV
from LAB06.movie_data import MovieData

class MoviePredictions:
    def __init__(self, data: MovieData):
        self.data = data
        self.algo = self.calculate_recomendations(data.df)


    def calculate_recomendations(self, df: pd.DataFrame):
        reader = Reader(rating_scale=(1,10))
        data = Dataset.load_from_df(df[['osoba', 'film', 'ocena']], reader)
        algo = GridSearchCV(KNNWithMeans, refit=True, cv=3, measures=["rmse", "mae"], param_grid={
            "sim_options": {
                "name": ["msd", "cosine"],
                "min_support": [3, 4, 5],
                "user_based": [False, True],
            }
        })
        algo.fit(data)
        return algo


    def predict(self, person, movie):
        if self.data.check_if_exists(person, movie):
            prediction = self.algo.predict(person, movie)
            return prediction.est

