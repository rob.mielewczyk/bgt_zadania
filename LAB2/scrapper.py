'''
Autor: Robert Mielewczyk
1. Znaleźć wiersze Szymona Zimorowica
2. Dokonać analizy
- 10 najpopularniejszych słów
- 3 najpopularniejsze rzeczowniki
'''


from bs4 import BeautifulSoup
from collections import Counter
import requests
import re

class PoemScrapper:
    URL = "http://cyfrowa.chbp.chelm.pl/dlibra/plain-content?id=10520"

    def get_poems(self):
        html = BeautifulSoup(requests.get(self.URL).text)
        return [pre.text for pre in html.find_all('pre')]

    @staticmethod
    def clean_html(raw_html):
        cleanr = re.compile('<.*?>|[0-9]|[!~`-]|[\\r?\\n|\\t\_]')
        cleantext = re.sub(cleanr, '', raw_html)
        return cleantext


if __name__ == '__main__':
    scrapper = PoemScrapper()
    poems = scrapper.get_poems()

    cleaned_poems = []
    for poem in poems:
        cleaned_poems.append(
            scrapper.clean_html(poem)
        )

    # More cleaning ... Count
    all_words = [cleaned_poem.split(' ') for cleaned_poem in cleaned_poems]
    all_words_cleaned = []
    for words in all_words:
        all_words_cleaned = all_words_cleaned + words
    count = Counter(all_words_cleaned)





